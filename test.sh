#!/bin/bash

declare -A COUNT=()

SVC=$(minikube service front --url)

for i in `seq 1 100`; do
  RES=$(http $SVC)
  echo "[$i] $RES"
  ((COUNT[$RES]++))
done

echo ${COUNT[*]}
